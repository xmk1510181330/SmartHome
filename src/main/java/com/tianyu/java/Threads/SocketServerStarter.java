package com.tianyu.java.Threads;

import com.tianyu.java.Server.SmartHomeServer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Repository;

/**
 * 描述: 服务端进程的启动函数启动器
 */
@Repository
public class SocketServerStarter implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        SmartHomeServer server = new SmartHomeServer();
        server.start();
    }
}
