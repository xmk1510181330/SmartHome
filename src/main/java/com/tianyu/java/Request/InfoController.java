package com.tianyu.java.Request;


import com.tianyu.java.Message.EnvironmentInfo;
import com.tianyu.java.Message.LiveStockInfo;
import com.tianyu.java.Message.MessageInfo;
import com.tianyu.java.Message.SmartHomeInfo;
import com.tianyu.java.Server.CommonDataTool;
import com.tianyu.java.Server.Handler.SmartHomeWriteHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;

@RestController
@RequestMapping("/info")
public class InfoController {

    @Autowired
    CommonDataTool dataTool;

    @GetMapping("/get")
    public SmartHomeInfo getiInfo(@RequestParam String mac) {
        return (SmartHomeInfo) dataTool.getInfo(mac);
    }

    @GetMapping("/getLiveStock")
    public LiveStockInfo getInfo(@RequestParam String mac) {
        LiveStockInfo stockInfo = (LiveStockInfo) dataTool.getOtherInfo(mac);
        return stockInfo;
    }

    @GetMapping("/getEnvironment")
    public EnvironmentInfo getEnvironInfo(@RequestParam String mac) {
        EnvironmentInfo environmentInfo = (EnvironmentInfo) dataTool.getOtherInfo(mac);
        return environmentInfo;
    }


    @GetMapping("/operate")
    public String operateCurtain(@RequestParam Integer state, @RequestParam String mac, @RequestParam String object) {

        if(dataTool.getSocketChannel(mac) != null) {
            byte[] result = {(byte) 0xef, 0x33, 0x00};
            if(object.equals("Cu")) { //窗帘相关
                if(state == 0) {
                    result = new byte[]{(byte) 0xef, 0x22, 0x00};
                }
            }
            if(object.equals("Li")) { //灯光相关
                if(state == -1) {  //自动挡
                    result = new byte[]{(byte) 0xef, 0x55, 0x00};
                }else if(state == 0) { //关灯
                    result = new byte[]{(byte) 0xef, 0x44, 0x00};
                }else if(state > 0) { //亮度调节
                    result = new byte[]{(byte) 0xef, 0x44, state.byteValue()};
                }else { //未知
                    result = new byte[]{(byte) 0xef, 0x55, 0x00};
                }
            }
            dataTool.getSocketChannel(mac).write(ByteBuffer.wrap(result), null, new SmartHomeWriteHandler());
        }

        return "OK";
    }

    @GetMapping("operateLive")
    public String operateLive(@RequestParam Integer state, @RequestParam String mac, @RequestParam String object) {
        if(dataTool.getSocketChannel(mac) != null) {
            byte[] result = {(byte) 0xef, 0x33, 0x00};
            if(object.equals("Cu")) {
                if(state == 0) { //关窗户
                    result = new byte[]{(byte) 0xef, 0x43, 0x00};
                }else if(state == 1) {
                    result = new byte[]{(byte) 0xef, 0x44, 0x00};
                }else {
                    result = new byte[]{(byte) 0xef, 0x4f, 0x00};
                }
            }else if(object.equals("Li")) {
                if(state == 0) { //灯光
                    result = new byte[]{(byte) 0xef, 0x55, 0x00};
                }else if(state == -1) {
                    result = new byte[]{(byte) 0xef, 0x5f, 0x00};
                }else {
                    result = new byte[]{(byte) 0xef, 0x55, state.byteValue()};
                }
            }else if(object.equals("Va")) {
                if(state == 0) { //电磁阀
                    result = new byte[]{(byte) 0xef, 0x32, 0x00};
                }else if(state == 1) {
                    result = new byte[]{(byte) 0xef, 0x33, 0x00};
                }else {
                    result = new byte[]{(byte) 0xef, 0x3f, 0x00};
                }
            }else if(object.equals("Fa")) {
                if(state == 0) { //电磁阀
                    result = new byte[]{(byte) 0xef, 0x21, 0x00};
                }else if(state == 1) {
                    result = new byte[]{(byte) 0xef, 0x22, 0x00};
                }else {
                    result = new byte[]{(byte) 0xef, 0x2f, 0x00};
                }
            }else {

            }
            dataTool.getSocketChannel(mac).write(ByteBuffer.wrap(result), null, new SmartHomeWriteHandler());
        }
        return "OK";
    }

    @GetMapping("getMacs")
    public List<String> getMacStrs() {
        return dataTool.getMacStrs();
    }
}
