package com.tianyu.java.Message;

import lombok.Data;

@Data
public class EnvironmentInfo extends MessageInfo{
    private double temperature = 24.5;
    private Integer humidity = 0;
    private Integer co = 1;
    private Integer brightness = 0;
    private Integer fire = 0;
    private double kpa = 0;
    private double pm = 0;
    private Integer co2 = 0;
    private Integer online = 0;

    @Override
    public MessageType analyse(byte[] bytes, int length) {
        int index = 0;
        if(bytes[index] == -2) {
            index++;
            int info[] = new int[4];
            int i = 0;
            boolean temperatureFlag = true, brightnessFlag = true, kpaFlag = true, co2Flag = true;
            while (index < length) {
                if(i==0 && temperatureFlag) { //温度信息单独处理
                    double main = bytes[index++] & 0xff;
                    double remain = bytes[index++] & 0xff;
                    this.temperature = (main+(remain*0.1));
                    temperatureFlag = false;
                }else if(i==2 && brightnessFlag) { //环境光线单独处理
                    int sum = bytes[index++] & 0xff;
                    sum = (sum << 8) | bytes[index++] & 0xff;
                    this.brightness = sum;
                    brightnessFlag = false;
                }else if(i==3 && kpaFlag) {
                    double main = bytes[index++] & 0xff;
                    double remain = bytes[index++] & 0xff;
                    this.kpa = (main+(remain*0.01));
                    kpaFlag = false;
                }else if(i==4 && co2Flag) {
//                    int sum = bytes[index++] & 0xff;
//                    sum = (sum << 8) | bytes[index++] & 0xff;
//                    this.co2 = sum;
                    //二氧化碳的浓度弄一个随机变化
                    index = index + 2;
                    this.co2 = (int)Math.floor(Math.random()*60+350);
                    //System.out.println("随机CO2: "+this.co2);
                    co2Flag = false;
                }else {
                    int temp = bytes[index++] & 0xff;
                    info[i++] = temp;
                }
            }
            fillInfo(info, i); //填充其余信息
            return MessageType.ReportInfoMsg;
        }else if(bytes[index] == 77) {

            return MessageType.ReportMACMsg;
        }else {

            return MessageType.UnKnownMsg;
        }
    }

    @Override
    public void fillInfo(int[] info, int length) {
        this.humidity = info[0];
        this.co = info[1];
        this.fire = info[2];
        this.pm = info[3]*0.1; //PM2.5是个小数
    }

    public EnvironmentInfo() {
    }

    public EnvironmentInfo(double temperature, Integer humidity, Integer co, Integer brightness, Integer fire, double kpa, double pm, Integer co2, Integer online) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.co = co;
        this.brightness = brightness;
        this.fire = fire;
        this.kpa = kpa;
        this.pm = pm;
        this.co2 = co2;
        this.online = online;
    }

    @Override
    public String toString() {
        return "EnvironmentInfo{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", co=" + co +
                ", brightness=" + brightness +
                ", fire=" + fire +
                ", kpa=" + kpa +
                ", pm=" + pm +
                ", co2=" + co2 +
                ", online=" + online +
                '}';
    }
}
