package com.tianyu.java.Message;

/**
 * 下位机上报消息的类型枚举
 */
public enum MessageType {
    ConnectMsg(101, "连接指令"),
    ReportMACMsg(102, "MAC地址上报指令"),
    ReportInfoMsg(103, "消息上报指令"),
    UnKnownMsg(100, "未知消息"),
    ;
    private Integer type;
    private String description;

    MessageType(Integer type, String description) {
        this.type = type;
        this.description = description;
    }
}
