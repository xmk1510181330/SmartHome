package com.tianyu.java.Message;

import lombok.Data;

import java.io.Serializable;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * 畜牧牲畜相关信息
 */
@Data
public class LiveStockInfo extends MessageInfo {
    private double temperature = 0;
    private Integer humidity = 0;
    private Integer nh3 = 1;
    private Integer brightness = 0;
    private Integer curtain = 0;
    private Integer valve = 0;
    private Integer fan = 0;
    private Integer online = 0;

    @Override
    public MessageType analyse(byte bytes[], int length) {
        int index = 0;
        if(bytes[index] == -2) {
            index++;
            int info[] = new int[5];
            int i = 0;
            boolean temperatureFlag = true, brightness = true;
            while (index < length) {
                if(i==0 && temperatureFlag) { //温度信息单独处理
                    double main = bytes[index++] & 0xff;
                    double remain = bytes[index++] & 0xff;
                    this.temperature = (main+(remain*0.1));
                    temperatureFlag = false;
                }else if(i==2 && brightness) { //环境光线单独处理
                    int sum = bytes[index++] & 0xff;
                    sum = (sum << 8) | bytes[index++] & 0xff;
                    this.brightness = sum;
                    brightness = false;
                }else {
                    int temp = bytes[index++] & 0xff;
                    info[i++] = temp;
                }
            }
            fillInfo(info, i); //填充其余信息
            return MessageType.ReportInfoMsg;
        }else if(bytes[index] == 77) {

            return MessageType.ReportMACMsg;
        }else {

            return MessageType.UnKnownMsg;
        }
    }

    @Override
    public void fillInfo(int[] info, int length) {
        this.humidity = info[0];
        this.nh3 = info[1];
        this.curtain = info[2];
        this.valve = info[3];
        this.fan = info[4];
    }

    public LiveStockInfo() {
    }

    @Override
    public String toString() {
        return "LiveStockInfo{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", NH3=" + nh3 +
                ", brightness=" + brightness +
                ", curtain=" + curtain +
                ", valve=" + valve +
                ", fan=" + fan +
                '}';
    }
}
