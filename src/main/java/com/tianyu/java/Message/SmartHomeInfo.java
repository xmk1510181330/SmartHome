package com.tianyu.java.Message;

import lombok.Data;
import org.springframework.stereotype.Repository;

/**
 * 描述: 智能家居下位机上报上来的信息
 */
@Data
public class SmartHomeInfo extends MessageInfo{
    /** 温度*/
    private Integer temperature;
    /** 温度*/
    private Integer humidity;
    /** 气体浓度*/
    private Integer gas;
    /** 光照强度*/
    private Integer brightness;
    /** 灯光情况*/
    private Integer light;
    /** 窗帘状态*/
    private Integer curtain;
    /** 下位机状态*/
    private Integer online;

    /** 设置数据*/
    public void setInfo(int[] infos) {
        if(infos.length == 7) {  //数量正确
            this.temperature = infos[0];
            this.humidity = infos[1];
            this.gas = infos[2];
            this.brightness = infos[3];
            this.light = infos[4];
            this.curtain = infos[5];
            this.online = infos[6];
        }
    }

    @Override
    public MessageType analyse(byte[] bytes, int length) {
        return null;
    }

    @Override
    public void fillInfo(int[] info, int length) {

    }

    public SmartHomeInfo() {
    }

    public SmartHomeInfo(Integer temperature, Integer humidity, Integer gas, Integer brightness, Integer light, Integer curtain, Integer online) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.gas = gas;
        this.brightness = brightness;
        this.light = light;
        this.curtain = curtain;
        this.online = online;
    }

    @Override
    public String toString() {
        return "SmartHomeInfo{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", gas=" + gas +
                ", brightness=" + brightness +
                ", light=" + light +
                ", curtain=" + curtain +
                ", online=" + online +
                '}';
    }
}
