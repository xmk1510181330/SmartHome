package com.tianyu.java.Message;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * 描述: 智能家居方面消息传递的基类
 */
public class SmartHomeMessage implements Serializable {
    /** 魔数，用以过滤一些垃圾信息*/
    static String magic = "zzuli";
    /** 协议版本号，用来区分不同版本的通信协议*/
    static String version = "1.0.1";

    public static byte[] getMagicContent() {
        return magic.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] getVersionContent() {
        return version.getBytes(StandardCharsets.UTF_8);
    }
}
