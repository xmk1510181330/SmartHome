package com.tianyu.java.Message;

import java.io.Serializable;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * @作者: mingke
 * @描述: 上下位机信息传递的基类
 * @日期: 2022-5-3
 */
public abstract class MessageInfo implements Serializable {
    private String application; //应用场景
    private String version; //通讯协议版本

    /**
     * @desc 分析数据的方法
     * @param bytes 下位机上报的数据本体
     * @param length 数据长度
     * @return
     */
    public abstract MessageType analyse(byte bytes[], int length);

    /**
     * @desc 处理消息的方法
     * @param type 消息的类型
     * @param addtionParam 附加参数，如必要，携带一些内容
     */
    //public abstract void handler(MessageType type, String addtionParam);

    /**
     * @desc 填充信息的方法
     * @param info 得到的信息数组
     * @param length 信息数组的长度
     */
    public abstract void fillInfo(int info[], int length);
}
