package com.tianyu.java;

import com.tianyu.java.Server.SmartHomeContextTool;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SmartHomeContextTool.instance.setContext(SpringApplication.run(Main.class, args));
    }
}
