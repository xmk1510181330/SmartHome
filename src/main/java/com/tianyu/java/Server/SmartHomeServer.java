package com.tianyu.java.Server;

import com.tianyu.java.Server.Handler.SmartHomeAcceptHandler;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;

/**
 * 描述: 服务端启动函数
 */
public class SmartHomeServer {
    public void start() {
        //设置服务器的端口号
        InetSocketAddress inetSocketAddress = new InetSocketAddress(2022);
        try {
            final AsynchronousServerSocketChannel channel = AsynchronousServerSocketChannel.open();
            channel.bind(inetSocketAddress);
            System.out.println("服务器启动完毕");
            channel.accept(channel, new SmartHomeAcceptHandler());
        }catch(Exception e) {
            e.printStackTrace();
        }
        //弄一个线程过来的目的是不让当前这个服务端直接结束
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
    }
}
