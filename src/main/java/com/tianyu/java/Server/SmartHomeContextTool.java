package com.tianyu.java.Server;

import org.springframework.context.ApplicationContext;

public class SmartHomeContextTool {

    /**
     * 逻辑处理器需要使用SpringBoot托管的类，但是AIO请求处理部分的并没有被Spring托管
     * 这里要用Redis和Mybatis相关的内容可以通过持有Spring的上下文来操作
     */
    /** 全局单例*/
    public final static SmartHomeContextTool instance = new SmartHomeContextTool();
    /** 持有一个上下文对象*/
    public ApplicationContext context;

    /** 私有化构造器*/
    private SmartHomeContextTool() {
    }

    public ApplicationContext getContext() {
        return context;
    }

    public void setContext(ApplicationContext context) {
        this.context = context;
    }
}
