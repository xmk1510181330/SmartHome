package com.tianyu.java.Server.Handler;


import java.nio.channels.CompletionHandler;

public class SmartHomeWriteHandler implements CompletionHandler<Integer, Void> {

    @Override
    public void completed(Integer result, Void attachment) {
        System.out.println("消息投递成功");
    }

    @Override
    public void failed(Throwable exc, Void attachment) {
        System.out.println("消息投递失败");
    }
}
