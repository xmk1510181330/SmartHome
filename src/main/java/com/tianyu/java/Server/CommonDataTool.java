package com.tianyu.java.Server;

import com.tianyu.java.Message.EnvironmentInfo;
import com.tianyu.java.Message.LiveStockInfo;
import com.tianyu.java.Message.MessageInfo;
import com.tianyu.java.Message.SmartHomeInfo;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Repository
public class CommonDataTool {
    //数据键
    static String key = "magic";
    //数据中心
    private HashMap<String, MessageInfo> dataMap = new HashMap<>();
    //当前维持通信的信道
    private AsynchronousSocketChannel useSocketChannel;
    //当前与客户机进行数据交互的MAP，键是硬件设备的MAC地址，值是通讯信道
    private HashMap<String, AsynchronousSocketChannel> clients = new HashMap<>();
    //通讯信道的哈希和MAC的MAP，便于根据信道找到MAC
    private HashMap<Integer, String> macHash = new HashMap<>();
    //指定MAC和最后一跳信息的时间哈希
    private HashMap<String, Long> heartHash = new HashMap<>();

    //获取数据
    public MessageInfo getInfo(String mac) {
        if(IsOnLine(mac)) {
            return dataMap.get(mac);
        }else {
            if(mac.startsWith("MAC")) {
                return new SmartHomeInfo(23, 70, 1, 1, 2, 3, 0);
            }else if(mac.startsWith("XMYZ")) {
                return new LiveStockInfo();
            }else if(mac.startsWith("HJJC"))  {
                return new EnvironmentInfo();
            }else {
                return null;
            }

        }
    }

    public MessageInfo getOtherInfo(String mac) {
        if(IsOnLine(mac)) {
            return dataMap.get(mac);
        }else {
            if(mac.startsWith("MAC")) {
                return new SmartHomeInfo(23, 70, 1, 1, 2, 3, 0);
            }else if(mac.startsWith("XMYZ")) {
                return new LiveStockInfo();
            }else if(mac.startsWith("HJJC"))  {
                return new EnvironmentInfo();
            }else {
                return null;
            }
        }
    }

    //设置内容
    public void setInfo(MessageInfo info, String mac) {
        dataMap.put(mac, info);
    }

    public CommonDataTool() {
    }

    public void setHeart(String mac) {
        heartHash.put(mac, System.currentTimeMillis());
    }

    public boolean IsOnLine(String mac) {
        long nowMillis = System.currentTimeMillis();
        if(!heartHash.containsKey(mac)) {
            return false;
        }else if(nowMillis-heartHash.get(mac)>2500) {
            dataMap.remove(mac);
            return false;
        }else {
            return true;
        }
    }


    public void setSocketChannel(String mac, AsynchronousSocketChannel socketChannel) {
        //存储MAC地址和信道的映射关系
        clients.put(mac, socketChannel);
        //存储哈希码和MAC的对应关系
        macHash.put(socketChannel.hashCode(), mac);
        //信息里面给一个初始化信息
        if(mac.startsWith("MAC")) { //智能家居的板子
            dataMap.put(mac, new SmartHomeInfo(23, 70, 1, 1, 2, 3, 1));
        }else if(mac.startsWith("XMYZ")) { //畜牧养殖的板子
            dataMap.put(mac, new LiveStockInfo());
        }else if(mac.startsWith("HJJC")) {
            dataMap.put(mac, new EnvironmentInfo());
        }

    }

    public AsynchronousSocketChannel getSocketChannel(String mac) {
        return clients.get(mac);
    }

    public String getMac(AsynchronousSocketChannel socketChannel) {
        Integer hashcode = socketChannel.hashCode();
        return macHash.get(hashcode);
    }

    //移除对应的信道
    public void removeSocketChannel(AsynchronousSocketChannel socketChannel) {
        //移除信道
        String macStr = macHash.get(socketChannel.hashCode());
        clients.remove(macStr);
        //关闭信道
        try {
            socketChannel.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //移除数据集合里面的数据
        dataMap.remove(macStr);
    }

    public List<String> getMacStrs() {
        List<String> result = new ArrayList<>();
        dataMap.keySet().forEach(item->{
            result.add(item);
        });
        return result;
    }
}
